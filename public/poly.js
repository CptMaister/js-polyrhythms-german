/// settings

// const startAnimationAtMilli = -15000; // TODO fix moveLines Pulse active at start and other bugs with earlier starting time
const startAnimationAtMilli = 0;
let startTimeMilli = new Date().getTime(); // time at which the animation starts, so now
let zeroTimeMilli = startTimeMilli - startAnimationAtMilli; // time at which the animation is at its reset point

const currentAmountOfSoundBites = 21; // this is not really a variable, but the amount of sound bites currently saved (per instrument) // TODO remove this completely

const singleWayTimeMilli = 1000; // this is exactly 1000 because we decided that moveLinePositionAtTime functions need time 0 to 1000 to get from one pulse line to the next
const singleWayTimeSeconds = 1;

const instruments = {
  vibraphone: { // from Hyperplexed's video
    type: "files",
    amount: 21,
    pathStart: "vibraphone/vibraphone-key-",
    pathEnd: ".mp3",
    volume: 0.1
  },
  orchestralHarp: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "orchestral-harp.json",
    volume: 0.9
  },
  kalimba: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "kalimba.json",
    volume: 0.7
  },
  timpani: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "timpani.json",
    volume: 0.6
  },
  tubularBells: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "tubular-bells.json",
    volume: 0.6
  },
  acousticGrandPiano: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "acoustic-grand-piano.json",
    volume: 0.6
  },
  acousticGuitarNylon: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "acoustic-guitar-nylon.json",
    volume: 0.6
  },
  xylophone: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "xylophone.json",
    volume: 0.6
  },
  violin: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "violin.json",
    volume: 0.6
  }

  // TODO check https://github.com/surikov/webaudiofont
}

const mainSettings = {
  instrument: instruments.vibraphone, // choose from instruments object above

  moveLinesAmount: 21, // this should not be higher than currentAmountOfSoundBites

  reverseMoveLinesOrder: true,
}

const soundSettings = {
  audioVolume: 1, // will be multiplied with by each instrument's volume

  reverseSoundsOrder: false,

  directionalSound: false, // TODO implement
}

const durationSettings = {
  pulsesAmountByFirstCircle: 100, // amount of pulse lines (i.e. sounds) the first circle will pass after fullAnimationTimeSeconds many seconds. 100 is a good one

  pulsesDifferenceStyle: "sub", // "sub" | "add" | "mult" | "div" // set how the amount of cleared pulses will be different for each following circle
  pulsesDifferenceNumber: 2, // difference of amount of loops between circles

  fullAnimationTimeSeconds: 450, // 900 seconds is 15 minutes
  fullAnimationsAmount: 0, // after this many full animation loops, the whole thing stops. Use 0 for unlimited. TODO use
  generalAnimationSpeedMultiplier: 1, // just make things faster or slower, with 1 being 100% speed // TODO make this work properly after loop logic redesign
}

const variationSettings = { // TODO implement everything here
  variation: "polygon", // "linear" | "polygon" | "circle" | "radial" | "dvd" // TODO use

  variationLinearPulseLinesAmount: 1,
  variationLinearPulseAtLimit: true, // at false the pulse lines are not placed at the limits, at true the first one is at the limits, with the two limit pulse lines acting as one pulse line
  variationLinearDirection: "right", // "right" | "top" | "left" | "bottom"
  variationLinearStartAtOtherEnd: false,
  variationLinearBounce: true, // go back from the end, instead of teleporting to the start. If variationLinearPulseAtLimit is true, then the limit Pulse will be played on either end
  variationLinearRotation: 0, // in degrees, at 0 the spheres go left to right // TODO implement
  variationLinearHeightScale: 1,
  variationLinearWidthScale: 1,

  variationPolygonEdgesAmount: 6, // at least 3 is nice, 1 and 2 also work
  variationPolygonPulseLineDivisor: 1, // should be a divisor of variationPolygonEdgesAmount, handle in FrontEnd
  variationPolygonReverseDirection: false, // for false go counter-clockwise, for true clockwise
  variationPolygonMoveLinesAtLimit: false, // at false the pulse lines are not placed at the limits, at true the first and last one is at the limits
  variationPolygonCenterEmpty: 0.1, // inner empty space, between 0 and 1, where 1 is the whole size
  variationPolygonCenterPulseLinesExtend: false, // makes the pulse lines extend into the center
  variationPolygonBounce: false,
  variationPolygonDelivery: false, // emulates the trick from the video https://www.youtube.com/watch?v=v46MnsZjPNo // recommended to use pulseLinesVisible false with this one
  variationPolygonDeliveryInverted: false, // only works if variationPolygonDelivery is true, inverts the delivery effect
  variationPolygonRotation: 0, // in degrees. At 0, the spheres start at the top, going in counter-clockwise rotational direction
  variationPolygonRotationSpeed: 0, // number of rotations that the whole polygon will finish during the time fullAnimationTimeSeconds, divided by the amount of edges
  variationPolygonScale: 1, // at 1 the length of one pulseLine is half of (0.8 * minDim), with minDim equal to Math.min(viewportWidth, viewportHeight)

  variationCircleArcRadians: 90, // in degrees, between 1 and 360
  variationCirclePulseAtLimit: true, // at false the pulse lines are not placed at the limits, at true the first one is at the limits, with the two limit pulse lines acting as one pulse line
  variationCircleStartAtOtherEnd: false, // at false the spheres start at the right end of the arcs, at true at the left
  variationRadialCenterEmpty: 0.1, // inner circle empty space, between 0 and 1, where 1 is the size of the outer circle
  variationRadialCenterPulseLinesExtend: false, // makes the pulse lines extend into the center
  variationCircleBounce: true, // if false, circles will just teleport to their start, which only looks natural if variationCircleArcRadians is a multiple of 360, else true is recommended here
  variationCircleBounceAdjusted: true, // if variationCircleBounce is true and variationCircleBounceAdjusted is true, then the sphereWidth will be used to slightly offset the spheres' bounce position so that the spheres actually bounce with their edge against the arc ends, instead of bouncing with their centers. Will be ignored if variationCircleBounce is false
  variationCircleRotation: 0, // in degrees. At 0, the center of the arc is at the top. The spheres start at the right end of the arcs.
  variationCircleScale: 1, // at 1 the diameter of the animation is (0.8 * minDim), with minDim equal to Math.min(viewportWidth, viewportHeight)

  variationRadialArcRadians: 90, // in degrees, emitting area of spheres, any number from 1 is good, values above 360 are different from their (mod 360)-equivalent
  variationRadialStartAtOutside: false, // at false the spheres start at the center, at true at the outside
  variationRadialPulseAtOutside: true, // add a pulse at the outside. Does not change the pulses on the inside
  variationRadialPulseAtCenter: false, // add a pulse at the center. Does not change the pulses on the inside
  variationRadialCenterRadius: 0, // inner circle size, between 0 and 1, where 1 is the size of the outer circle
  variationRadialBounceAtCenter: true, // will always be true if variationRadialCenterRadius is higher than 0
  variationRadialRotation: 0, // in degrees. At 0, the center of the arc is at the top. The spheres start at the right end.
  variationRadialScale: 1, // at 1 the diameter of the animation is (0.8 * minDim), with minDim equal to Math.min(viewportWidth, viewportHeight)

  variationDvdWidth: 16, // let's only do whole numbers here
  variationDvdHeight: 9, // let's only do whole numbers here
  variationDvdSlopeX: 1, // let's only do whole numbers here
  variationDvdSlopeY: 1, // let's only do whole numbers here
  variationDvdSpheresAmount: 1,
  variationDvdTrace: false,
  variationDvdSoundsPerSphere: 1,
  variationDvdImpactColorChange: false,
  variationDvdScale: 1,
}

const styleSettings = {
  backgroundColor: "black",
  textColor: "white",

  sphereShape: "circle", // "circle" | "square" | "triangle" | "indexNumber" | "numberOfImpacts"
  sphereColor: "white",
  sphereOutline: "white",
  sphereWidth: 8,
  spherePulseColor: false,

  connectionLineSpheres: "", // "neighbors" | "center" | "spheres" | anything else for no connection lines // draw a straight line connecting each sphere with its up to two index neighbor spheres, with the center of the animation, or with all other spheres
  connectionLineColor: "white",
  connectionLineWidth: 1.5,

  pulseLinesVisible: true,
  pulseLineColor: "white",
  pulseLineWidth: 6,

  moveLinesVisible: true,
  moveLineColorHSLHue: "colorGradientReverse", // "colorGradient" | "colorGradientReverse" | any hue value of the color wheel, from 0 to 360, see   // e.g. "gray" or "#00ff00"
  moveLineColorHSLSaturation: 1,
  moveLineWidth: 4,

  fadeOutImpactPulseTimeMilli: 400,
  moveLineSilenceOpacity: 0.2,
  moveLineSilenceLightness: 0.5,
  moveLinePulseOpacity: 1.0,
  moveLinePulseLightness: 0.6,
  moveLinePulseWidthExtra: 4,
}



/// one time only setup stuff

// html elements
const body = document.getElementById("body");
const paper = document.getElementById("paper");
const pen = paper.getContext("2d"); // see https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D

const divUi = document.getElementById("divUi");

const spanTimer = document.getElementById("spanTimer");

const buttonSoundToggle = document.getElementById("buttonSoundToggle");
const buttonSettingsMenu = document.getElementById("buttonSettingsMenu");
const spanInfoToggleSound = document.getElementById("spanInfoToggleSound");
const buttonRestartAnimation = document.getElementById("buttonRestartAnimation");
const buttonVisibilityToggle = document.getElementById("buttonVisibilityToggle");
const buttonTimerSinceStart = document.getElementById("buttonTimerSinceStart");


// styling stuff
body.style.backgroundColor = styleSettings.backgroundColor;
body.style.color = styleSettings.textColor;


// audio
let audioContext;
let gainNode;

function initializeAudioContext() {
  try {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();

    gainNode = audioContext.createGain();
    gainNode.gain.value = mainSettings.instrument.volume * soundSettings.audioVolume; // audio volume
    gainNode.connect(audioContext.destination);

    try {
      loadAudioBuffers();
    } catch(e) {
      console.warn("Loading audio files failed.")
    }
  } catch(e) {
    console.warn('Web Audio API is not supported in this browser');
  }
}

async function loadAudioBuffers() {

  const instrument = mainSettings.instrument;

  // build promises to get audioBuffers
  let audioBufferPromises;

  if (instrument.type === "files") {
    audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
      return fetchAudioBufferPromiseFromFile(getAudioPath(index));
    })

  } else if (instrument.type === "base64") {
    const instrumentSoundsPromise = fetch("./audio/" + instrument.path)
      .then((response) => response.json());
    const instrumentSounds = await instrumentSoundsPromise;

    audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
      return fetchAudioBufferPromiseFromBase64(instrumentSounds[
        soundSettings.reverseSoundsOrder ?
        instrumentSounds.length - (index % instrumentSounds.length) - 1 :
        (index % instrumentSounds.length)
      ]);
    })

  } else {
    console.warn("Instrument setup is messed up, unrecognized type.", instrument)
  }

  // wait for audio buffer promises and then add audioBuffers to paths
  Promise.all(audioBufferPromises).then(buffers => {
    const newPaths = paths.map((path, index) => ({
      ...path,
      audioBuffer: buffers[index]
    }));

    paths.splice(0, paths.length, ...newPaths)
  })
}

function fetchAudioBufferPromiseFromFile(path) {
  return fetch(path) // return this Promise
    .then((res) => res.arrayBuffer())
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer));
}


function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[arr.length - 1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, {type:mime});
}

function fetchAudioBufferPromiseFromBase64(base64) {
  const oggFile = dataURLtoFile(base64,`tmp-file-name.ogg`);
  return oggFile.arrayBuffer()
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer));
}

function getAudioPath(index) {
  const modIndex = index % mainSettings.instrument.amount;
  return `./audio/${
    mainSettings.instrument.pathStart
  }${
    soundSettings.reverseSoundsOrder ? (mainSettings.moveLinesAmount - modIndex - 1) : modIndex
  }${
    mainSettings.instrument.pathEnd
  }`
}

function playSound(buffer) {
  const source = audioContext.createBufferSource(); // creates a sound source
  source.buffer = buffer; // tell the source which sound to play
  // source.connect(audioContext.destination); // connect the source to the audioContext's destination (the speakers)
  source.connect(gainNode) // now instead of connecting to audioContext.destination, connect to the gainNode, to change volume -> see https://stackoverflow.com/a/43386415
  source.start(); // play the source now
}



// old audio
let soundEnabled = false;
updateButtonSoundToggle()
let soundEnabledOnce = false;

document.onvisibilitychange = () => toggleSound(false); // disable sound whenever window gets minimized, else sound will play all at once when you go back to the window, or the frame rendering will be off and the sound plays with weird timing. Not nice either way.
paper.onclick = () => toggleSound();
buttonSoundToggle.onclick = () => toggleSound();

function toggleSound(bool) {
  // initialize AudioContext, cannot be done before, browsers will block this as part of initially automatic running javascript
  if (audioContext === undefined) {
    initializeAudioContext();
  }
  soundEnabled = bool ?? !soundEnabled;
  updateButtonSoundToggle()
  if (soundEnabled && !soundEnabledOnce) {
    soundEnabledOnce = true;
    spanInfoToggleSound.style.display = "none";
  }
}
function updateButtonSoundToggle() {
  buttonSoundToggle.innerHTML = soundEnabled ? "🔊" : "🔇"
}


// some ui stuff
buttonVisibilityToggle.onclick = toggleUiVisibility;
let uiVisibile = true;
function toggleUiVisibility() {
  uiVisibile = !uiVisibile
  divUi.style.opacity = uiVisibile ? 1 : 0;
}

buttonRestartAnimation.onclick = restartAnimation;
function restartAnimation() {
  startTimeMilli = new Date().getTime();
  zeroTimeMilli = startTimeMilli - startAnimationAtMilli;
  elapsedStartSecondsFloored = -1; // -1 so that the slightly optimized update immediately increments to 0

  // reset impact times
  const newPaths = paths.map(path => {
    const currentTime = new Date().getTime();
    const lastImpactTime = calculateNextImpactTimeMilli(currentTime, zeroTimeMilli, path.velocity, true)
    const nextImpactTime = calculateNextImpactTimeMilli(currentTime, zeroTimeMilli, path.velocity)

    return {
      ...path,
      lastImpactTime,
      nextImpactTime
    }
  })
  paths.splice(0, paths.length, ...newPaths)
}

buttonTimerSinceStart.onclick = toggleTimerVisibility;
let timerVisible = true;
function toggleTimerVisibility() {
  timerVisible = !timerVisible
  spanTimer.style.opacity = timerVisible ? 1 : 0;
}
toggleTimerVisibility() // disable timer at first

let elapsedStartSecondsFloored = -1; // -1 so that the slightly optimized update immediately increments to 0
function updateTimer(elapsedSecondsRelativeToZeroTime) {
  if (!timerVisible) {
    return;
  }
  const adjustedElapsedSeconds = elapsedSecondsRelativeToZeroTime - (startAnimationAtMilli / 1000);
  if (adjustedElapsedSeconds-1 > elapsedStartSecondsFloored) {
    console.log("adjustedElapsedSeconds", adjustedElapsedSeconds)
    elapsedStartSecondsFloored = Math.floor(adjustedElapsedSeconds);
    spanTimer.innerHTML = `Timer: ${elapsedStartSecondsFloored >= 60 ? Math.floor(elapsedStartSecondsFloored / 60) + "m " : ""}${elapsedStartSecondsFloored % 60}s${startAnimationAtMilli === 0 ? "" : " (" + (startAnimationAtMilli > 0 ? "+" : "-") + (Math.abs(startAnimationAtMilli) / 1000) + "s)"}`;
  }
}




// prepare paths unchanging stuff: color, audio, velocity, impact times
let paths = generatePathHues(mainSettings.moveLinesAmount).map((hue, index) => {
  if (index >= currentAmountOfSoundBites) {
    console.warn("Warning, more moveLines than currentAmountOfSoundBites, so they probably have no sound.")
  }

  /// old way of creating the audio stuff, replaced by AudioContext:
  // const audio = new Audio(`./audio/${mainSettings.instrument}-key-${
  //   soundSettings.reverseSoundsOrder ? (mainSettings.moveLinesAmount - index - 1) : index
  // }.mp3`); // TODO: check https://web.dev/webaudio-intro/ for less laggy audio maybe
  // audio.volume = soundSettings.audioVolume;

  const numberOfPulsesPerFullAnimation = getNumberOfPulsesPerFullAnimation(index);
  const velocity = (singleWayTimeSeconds * numberOfPulsesPerFullAnimation) / durationSettings.fullAnimationTimeSeconds;

  const currentTime = new Date().getTime();
  lastImpactTime = calculateNextImpactTimeMilli(currentTime, zeroTimeMilli, velocity, true)
  const nextImpactTime = calculateNextImpactTimeMilli(currentTime, zeroTimeMilli, velocity)

  return {
    hue,
    // audio,
    audioBuffer: undefined,
    velocity,
    lastImpactTime,
    nextImpactTime
  }
});

function getNumberOfPulsesPerFullAnimation(index) {
  const first = durationSettings.pulsesAmountByFirstCircle;
  const number = durationSettings.pulsesDifferenceNumber;
  const style = durationSettings.pulsesDifferenceStyle;

  switch (style) {
    case "sub":
      return first - number * index;
    case "add":
      return first + number * index;
    case "div":
      return first / Math.pow(number, index);
    case "mult":
      return first * Math.pow(number, index);
    default:
      return first - number * index;
  }
}


/// main loop
function draw() {
  const currentTime = new Date().getTime();
  const elapsedTimeSeconds = (currentTime - zeroTimeMilli) / 1000;

  updateTimer(elapsedTimeSeconds);

  // in case of rescaling of the window, this will update the canvas size
  paper.width = paper.clientWidth;
  paper.height = paper.clientHeight;

  const animationCenter = {
    x: paper.width / 2,
    y: paper.height / 2
  };

  // draw pulse lines
  const pulseLinesAtTime = createPulseLines(animationCenter, elapsedTimeSeconds);

  // create paths
  const moveLinePositionAtTimeArray = createMoveLines(paths, pulseLinesAtTime, elapsedTimeSeconds, currentTime);

  // draw circles
  drawSpheres(animationCenter, paths, moveLinePositionAtTimeArray, elapsedTimeSeconds, currentTime);

  requestAnimationFrame(draw);
}

draw();



/// main functions
function calculateNextImpactTimeMilli(now, anyMomentOfImpact, velocity, actuallyCalculatePreviousImpactTime) {
  const impactDelta = singleWayTimeMilli / velocity;

  let revMult = 1;
  if (actuallyCalculatePreviousImpactTime === false) {
    revMult = -1; // this sneaky boy will change the following calculations to actually get the previous impact time
  }

  const timeUntilNextImpact = realMod(revMult * (anyMomentOfImpact - now), impactDelta);
  const adjustedTimeUntilNextImpact = (
    (timeUntilNextImpact === 0 && actuallyCalculatePreviousImpactTime === false) ?
    impactDelta :
    timeUntilNextImpact
  );
  const nextImpactTime = now + revMult * adjustedTimeUntilNextImpact;
  return nextImpactTime;
}


function createPulseLines(animationCenter, elapsedAnimationTime) {
  pen.strokeStyle = styleSettings.pulseLineColor;
  pen.lineWidth = styleSettings.pulseLineWidth;
  pen.lineCap = "round";

  if (variationSettings.variation === "linear") {
    const amount = variationSettings.variationLinearPulseLinesAmount;
    const atLimit = variationSettings.variationLinearPulseAtLimit;
    const dir = variationSettings.variationLinearDirection;

    const linearHeight = paper.height * variationSettings.variationLinearHeightScale;
    const linearWidth = paper.width * variationSettings.variationLinearWidthScale;

    const borderLeft = animationCenter - (linearHeight / 2);
    const borderRight = animationCenter + (linearHeight / 2);
    const borderTop = animationCenter - (linearWidth / 2);
    const borderBottom = animationCenter + (linearWidth / 2);

    const heightDelta = linearHeight / amount;
    const widthDelta = linearWidth / amount;

    for (let i = 0; i < amount + (atLimit ? 1 : 0); i++) {
      let start, end;

      if (dir === "right" || dir === "left") {
        const right = dir === "right";
        const iX = (right ? borderLeft : borderRight) + widthDelta * i + (right ? 1 : -1) * (atLimit ? 0 : (widthDelta / 2))
        start = {
          x: iX,
          y: borderTop
        }
        end = {
          x: iX,
          y: borderBottom
        }
      } else {
        const top = dir === "top";
        const iY = (top ? borderBottom : borderTop) + heightDelta * i + (top ? 1 : -1) * (atLimit ? 0 : (heightDelta / 2))
        start = {
          x: borderLeft,
          y: iY
        }
        end = {
          x: borderRight,
          y: iY
        }
      }

      // TODO rotate points if variationLinearRotation is not 0 (mod 360)

      if (styleSettings.pulseLinesVisible) {
        drawLine(start, end);
      }
    }

    return [];

  } else if (variationSettings.variation === "polygon") {
    const edges = variationSettings.variationPolygonEdgesAmount;
    const divisor = variationSettings.variationPolygonPulseLineDivisor;
    const reverseDir = variationSettings.variationPolygonReverseDirection;
    const centerEmpty = variationSettings.variationPolygonCenterEmpty;
    const centerPulseLines = variationSettings.variationPolygonCenterPulseLinesExtend;
    const rotation = variationSettings.variationPolygonRotation;
    const rotationSpeed = variationSettings.variationPolygonRotationSpeed;
    const scale = variationSettings.variationPolygonScale;

    const pulseLineAtTimeArray = [];

    const degreesDelta = 360 / edges;
    const neutralRotation = 90; // upwards, take care of y positive going down in html, not up
    const radius = (0.4 * Math.min(paper.width, paper.height)) * scale;
    const innerEmptyRadius = radius * centerEmpty * scale;
    const centerEmptyDiff = 1 - centerEmpty;

    for (let i = 0; i < edges; i++) {
      function iAngleAtAnimationTime(elAnimationTime) {
        const currentRotationFromRotationSpeed = rotationSpeed * (elAnimationTime / durationSettings.fullAnimationTimeSeconds) * (360 / edges);

        return degreeToRadians(- (neutralRotation + (reverseDir ? -1 : 1) * (rotation + degreesDelta * i) + currentRotationFromRotationSpeed)); // the starting minus sign is there to adjust for y positive values going down in html, instead of up
      }

      function startAtAnimationTime(elAnimationTime) {
        return {
          x: animationCenter.x + (centerPulseLines ? 0 : (Math.cos(iAngleAtAnimationTime(elAnimationTime)) * innerEmptyRadius)),
          y: animationCenter.y + (centerPulseLines ? 0 : (Math.sin(iAngleAtAnimationTime(elAnimationTime)) * innerEmptyRadius)),
        };
      }
      function endAtAnimationTime(elAnimationTime) {
        return {
          x: animationCenter.x + Math.cos(iAngleAtAnimationTime(elAnimationTime)) * radius,
          y: animationCenter.y + Math.sin(iAngleAtAnimationTime(elAnimationTime)) * radius
        };
      }

      if (
        styleSettings.pulseLinesVisible
        && i % divisor === 0
      ) {
        const startAtTheMoment = startAtAnimationTime(elapsedAnimationTime);
        const endAtTheMoment = endAtAnimationTime(elapsedAnimationTime);

        drawLine(startAtTheMoment, endAtTheMoment);
      }

      function pulseLineAtTime(time, elAnimationTime) {
        const handledElapsedAnimationTime = elAnimationTime ?? 0;
        const moduloTime = realMod(time, 1); // time loops after 1
        const startAtTheMoment = startAtAnimationTime(handledElapsedAnimationTime);
        const endAtTheMoment = endAtAnimationTime(handledElapsedAnimationTime);

        if (centerPulseLines && centerEmpty > 0) {
          return {
            x: ((1 - centerEmptyDiff * moduloTime) - centerEmpty) * startAtTheMoment.x + (centerEmptyDiff * moduloTime + centerEmpty) * endAtTheMoment.x, // linear interpolation
            // y: (1 - moduloTime) * startAtTheMoment.y + moduloTime * endAtTheMoment.y // linear interpolation
            y: ((1 - centerEmptyDiff * moduloTime) - centerEmpty) * startAtTheMoment.y + (centerEmptyDiff * moduloTime + centerEmpty) * endAtTheMoment.y // linear interpolation, with added empty center
          };
        } else {
          return {
            x: (1 - moduloTime) * startAtTheMoment.x + moduloTime * endAtTheMoment.x, // linear interpolation
            y: (1 - moduloTime) * startAtTheMoment.y + moduloTime * endAtTheMoment.y // linear interpolation
          };
        }
      }

      pulseLineAtTimeArray.push(pulseLineAtTime);
    }

    return pulseLineAtTimeArray;

  } else if (variationSettings.variation === "circle") {
    // TODO
  } else if (variationSettings.variation === "radial") {
    // TODO
  } else if (variationSettings.variation === "dvd") {
    // TODO
  }
}



function generatePathHues(amount) {
  const hue = styleSettings.moveLineColorHSLHue;
  if (hue === "colorGradient") {
    return generateHues(amount);
  } else if (hue === "colorGradientReverse") {
    return generateHues(amount, true);
  } else if (
    typeof hue !== "number"
    || hue < 0
    || hue > 360
  ) {
    return generateHues(amount).toReversed(); // default case
  } else {
    return Array(amount).fill(hue)
  }
}

function generateHues(amount, reverse) {
  const hues = [];
  const hueDelta = Math.trunc(360 / (amount -1)) // see https://mika-s.github.io/javascript/colors/hsl/2017/12/05/generating-random-colors-in-javascript.html

  for (let i = 0; i < amount; i++) {
    // const newHue = `hsla(${hueDelta * i}, 100%, 50%, 1.0)` // hsl color with hue depending on i, 100% saturation, 50% lightness (fulll color), 1.0 alpha (full opacity);
    hues.push(
      reverse ?
      (360 - hueDelta * i) :
      hueDelta * i
    );
  }

  return hues
}


function createMoveLines(paths, pulseLinesAtTime, elapsedTimeSeconds, currentTime) { // TODO CONTINUE currently only works for "polygon" variation
  pen.lineCap = "round";

  const amount = paths.length;

  const moveLinePositionAtTimeArray = [];

  const bounce = variationSettings.variationPolygonBounce;
  const edges = variationSettings.variationPolygonEdgesAmount;

  const bounceAdjustedEdges = bounce ? (edges * 2) : edges;

  const limit = variationSettings.variationPolygonMoveLinesAtLimit;

  for (let iterator = 0; iterator < amount; iterator++) {
    const adjustedIterator = (
      mainSettings.reverseMoveLinesOrder ?
      (amount - iterator - 1) :
      iterator
    );

    const positions = pulseLinesAtTime.map(pulseLineAtTime => {
      if (limit) {
        return pulseLineAtTime(adjustedIterator / amount, elapsedTimeSeconds);
      } else {
        return pulseLineAtTime((adjustedIterator + 1) / (amount + 1), elapsedTimeSeconds);
      }
    });

    if (styleSettings.moveLinesVisible) {
      const currentPath = paths[iterator];

      // handle visual line pulse
      const recentImpactTime = currentTime >= currentPath.nextImpactTime ? currentPath.nextImpactTime : currentPath.lastImpactTime;

      const pulseStrength = 1 - Math.max(Math.min((currentTime - recentImpactTime) / styleSettings.fadeOutImpactPulseTimeMilli, 1), 0);

      pen.lineWidth = styleSettings.moveLineWidth + pulseStrength * styleSettings.moveLinePulseWidthExtra;

      const opacity = pulseStrength * styleSettings.moveLinePulseOpacity + (1 - pulseStrength) * styleSettings.moveLineSilenceOpacity // linear interpolation
      const lightness = pulseStrength * styleSettings.moveLinePulseLightness + (1 - pulseStrength) * styleSettings.moveLineSilenceLightness // linear interpolation

      pen.strokeStyle = `hsla(${currentPath.hue}, ${styleSettings.moveLineColorHSLSaturation * 100}%, ${lightness * 100}%, ${opacity})` // hsl color with hue depending on i, 100% saturation, 50% lightness (full color), last one alpha between 0.0 and 1.0 (full transparency to full opacity);

      // draw line
      for (let j = 0; j < edges; j++) {
        drawLine(positions[j], positions[(j+1) % edges]);
      }
    }

    function moveLinePositionAtTime(time) { // time in seconds, as should always be assumed in this spaghetti code if it does not say "Milli"
      const moduloTime = realMod(
        durationSettings.generalAnimationSpeedMultiplier * time,
        bounceAdjustedEdges
      );
      const flooredModTime = Math.floor(moduloTime);
      const ceiledModTime = (flooredModTime + 1) % bounceAdjustedEdges;
      const modOneTime = moduloTime % 1;

      const bounceAdjustedFloored = (
        (bounce && flooredModTime >= edges) ?
        realMod(-flooredModTime, edges) :
        flooredModTime
      );
      const bounceAdjustedCeiled = (
        (bounce && ceiledModTime >= edges) ?
        realMod(-ceiledModTime, edges) :
        ceiledModTime
      );

      return {
        x: (1 - modOneTime) * positions[bounceAdjustedFloored].x + modOneTime * positions[bounceAdjustedCeiled].x, // linear interpolation
        y: (1 - modOneTime) * positions[bounceAdjustedFloored].y + modOneTime * positions[bounceAdjustedCeiled].y // linear interpolation
      }
    }

    moveLinePositionAtTimeArray.push(moveLinePositionAtTime);
  }

  return moveLinePositionAtTimeArray;
}

function getPulseStrengthForPath() { // TODO CONTINUE
  // const recentImpactTime = currentTime >= currentPath.nextImpactTime ? currentPath.nextImpactTime : currentPath.lastImpactTime;

  // const pulseStrength = 1 - Math.max(Math.min((currentTime - recentImpactTime) / styleSettings.fadeOutImpactPulseTimeMilli, 1), 0);
}


function drawSpheres(animationCenter, paths, moveLinePositionAtTimeArray, elapsedTimeSeconds, currentTime) {
  const amount = moveLinePositionAtTimeArray.length;
  const sphereCenters = [];

  for (let i = 0; i < amount; i++) {
    const moveLinePositionAtTime = moveLinePositionAtTimeArray[i];
    const currentPath = paths[i];

    const distance = currentPath.velocity * elapsedTimeSeconds;


    // draw polygon delivery mode spheres that are on the path of this current sphere // currently ignores the value of singleWayTimeMilliand just assumes that it is 1000, which it should probably be forever
    if (variationSettings.variation === "polygon" && variationSettings.variationPolygonDelivery) {
      const edges = variationSettings.variationPolygonEdgesAmount;
      const bounce = variationSettings.variationPolygonBounce;
      const inverted = variationSettings.variationPolygonDeliveryInverted;
      for (let j = 0; j < edges; j++) {
        const modDistance = realMod(distance, 2 * edges);
        const visible = (
          bounce ?
          (
            modDistance < j || (modDistance > edges && modDistance > realMod(-j, 2*edges))
          ) : (
            modDistance < j || modDistance > edges + j
          )
        )
        const invertedAdjustedVisible = (inverted ? !visible : visible)
        if (invertedAdjustedVisible) {
          const center = moveLinePositionAtTime(j);
          pen.beginPath();
          pen.fillStyle = styleSettings.sphereColor;
          pen.arc(center.x, center.y, styleSettings.sphereWidth /2, 0, 2 * Math.PI);
          pen.fill();
        }
      }
    }

    // save sphere centers
    const sphereCenter = moveLinePositionAtTime(distance);
    sphereCenters.push(sphereCenter);

    // play circle sound if hit the baseLine
    if (currentTime >= currentPath.nextImpactTime) {
      if (soundEnabled) {
        // currentPath.audio.play(); // old way

        playSound(currentPath.audioBuffer);
      }

      currentPath.lastImpactTime = currentPath.nextImpactTime;
      currentPath.nextImpactTime = calculateNextImpactTimeMilli(currentTime, currentPath.nextImpactTime, currentPath.velocity);
    }
  }

  // draw lines that connect spheres
  const sphereConnections = styleSettings.connectionLineSpheres;
  pen.strokeStyle = styleSettings.connectionLineColor;
  pen.lineWidth = styleSettings.connectionLineWidth;
  if (sphereConnections === "center") {
    sphereCenters.map(sphereCenter => {
      drawLine(animationCenter, sphereCenter);
    });
  } else if (sphereConnections === "neighbors") {
    for (let i = 0; i < sphereCenters.length - 1; i++) {
      drawLine(sphereCenters[i], sphereCenters[i + 1]);
    }
  } else if (sphereConnections === "spheres") {
    for (let i = 0; i < sphereCenters.length - 1; i++) {
      // WARNING: be careful with the n^2 many calculations needed here
      for (let j = i+1; j < sphereCenters.length; j++) {
        drawLine(sphereCenters[i], sphereCenters[j]);
      }
    }
  }

  // draw all spheres
  sphereCenters.map(sphereCenter => {
    pen.fillStyle = styleSettings.sphereColor;
    pen.beginPath();
    pen.arc(sphereCenter.x, sphereCenter.y, styleSettings.sphereWidth, 0, 2 * Math.PI);
    pen.fill();
  });
}





/// helpers
function realMod(number, modulus) {
  return ((number % modulus) + modulus) % modulus;
}

function degreeToRadians(degree) {
  return 2 * Math.PI * (degree / 360)
}

function drawLine(from, to) {
  pen.beginPath();
  pen.moveTo(from.x, from.y);
  pen.lineTo(to.x, to.y);
  pen.stroke();
}
# Polyrhythms in JavaScript

Result visible at [cptmaister.gitlab.io/js-polyrhythms/](https://cptmaister.gitlab.io/js-polyrhythms/)

Rebuilt project from [Hyperplexed](https://www.youtube.com/@Hyperplexed)'s video [Unravelling the Magic behind Polyrhythms](https://www.youtube.com/watch?v=Kt3DavtVGVE), with some adjustments by me. Check out his channel and also his [CodePen](https://codepen.io/Hyperplexed/pen/XWxqgGE?ref=Hyperplexed) as well as his 1 hour version video [1hr of Relaxing & Satisfying Polyrhythmic Spirals](https://www.youtube.com/watch?v=gvGfgCcfAEg).